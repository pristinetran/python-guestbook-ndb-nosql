from google.appengine.ext import ndb
import json

# [START greeting]
class Author(ndb.Model):
    """Sub model for representing an author."""
    identity = ndb.StringProperty(indexed=False)
    email = ndb.StringProperty(indexed=False)


class Greeting(ndb.Model):
    """A main model for representing an individual Guestbook entry."""
    author = ndb.StructuredProperty(Author)
    content = ndb.StringProperty(indexed=False)
    date = ndb.DateTimeProperty(auto_now_add=True)
# [END greeting]

# [START schoolsys]
class StudentModel(ndb.Model):
    """Sub model for representing a student_info"""
    name = ndb.StringProperty(indexed=False)
    name_lower = ndb.ComputedProperty(lambda self: self.name.lower())
    department = ndb.StringProperty(indexed=False)

    def __str__(self):
        return '%s %s %s' % (self.name, self.name_lower, self.department)

    def get_a_student_by_id(self, id=''):
        if id < 1:
            return None
        else:
            student = self.get_by_id(id)
            return student

    def get_a_student(self, name=''):
        student = self.gql('WHERE name_lower = :1', name.lower()).get()
        if student == "":
            return ''
        else:
            return student

    def get_student_id(self, name=''):
        student = self.get_a_student(name)
        if student == "":
            return 'No student found'
        else:
            return student.key.id()

    def get_student_info(self, name=''):
        if name == '':
            students = self.query().fetch(10)
            list = []
            result = map(lambda student: list.append({'id': student.key.id(), 'name':student.name, 'department': student.department}), students)
            return list
        else:
            student = self.get_a_student(name)
            dict = {
                'id': student.key.id(),
                'name': student.name,
                'name_lower': student.name_lower,
                'department': student.department
            }
            return dict
    # def del_student(self, name=''):

    def delete_student(self, name):
        student = self.get_a_student(name)
        if student == None:
            return 'null'
        else:
            id = student.key.id()
            print ("student.key %s", student.key)
            student.key.delete()
            return 'studentId %s removed!' % id


    def delete_all_students(self):
        students = self.query().fetch()
        if len(students) == 0:
            return
        else:
            map(lambda student: student.key.delete(), students)
            return

    # get_student_name

class CourseModel(ndb.Model):
    """A main model for representing a course_info"""
    name = ndb.StringProperty(indexed=False)
    name_lower = ndb.ComputedProperty(lambda self: self.name.lower())
    # student_list = ndb.KeyProperty(StudentModel, repeated=True)
    student_list = ndb.KeyProperty(kind=StudentModel, repeated=True)
    # date = ndb.DateTimeProperty(auto_now_add=True)
    department = ndb.StringProperty(indexed=False)

    def get_course_by_id(self, id):
        if id < 1:
            return None
        else:
            course = self.get_by_id(id)
            return course

    def get_course_by_name(self, name=''):
        course = self.gql('WHERE name_lower = :1', name.lower()).get()
        if course == "":
            return ''
        else:
            return course

    def delete_course(self, name):
        course = self.get_course_by_name(name)
        if course == None:
            return 'null'
        else:
            id = course.key.id()
            print ("course.key %s", course.key)
            course.key.delete()
            return 'courseId %s removed!' % id

    def delete_all_courses(self):
        courses = self.query().fetch()
        if len(courses) == 0:
            return
        else:
            map(lambda course: course.key.delete(), courses)
            return
# [END schoolsys]