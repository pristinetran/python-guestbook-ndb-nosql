#!/usr/bin/env python

# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START imports]
import os
import urllib

from google.appengine.api import users
from google.appengine.ext import ndb
from models import Author, Greeting, StudentModel, CourseModel
from collections import namedtuple

import jinja2
import webapp2
from webapp2_extras import json

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)
# [END imports]

# default value so API won't fail
DEFAULT_GUESTBOOK_NAME = 'default_guestbook'
DEFAULT_SCHOOLSYS_NAME = 'default_schoolsys'


# We set a parent key on the 'Greetings' to ensure that they are all
# in the same entity group. Queries across the single entity group
# will be consistent. However, the write rate should be limited to
# ~1/second.

def guestbook_key(guestbook_name=DEFAULT_GUESTBOOK_NAME):
    """Constructs a Datastore key for a Guestbook entity.

    We use guestbook_name as the key.
    """
    return ndb.Key('Guestbook', guestbook_name)

def schoolsys_key(schoolsys_name=DEFAULT_SCHOOLSYS_NAME):
    """Constructs a Datastore key for a SchoolSys entity.

    We use schoolsys_name as the key.
    """
    return ndb.Key('Schoolsys', schoolsys_name)

# [START seeding-data]
# extra, un-used method
def dict2obj(d):
    return namedtuple("Obj", d.keys())(*d.values())

"""
def seed_1st_student():
    entry = StudentModel()
    entry.populate(name="Kyle", department="Business")

seed_1st_student()
"""

StudentModel().delete_all_students()
CourseModel().delete_all_courses()
list_of_keys = []

def seed_student(student):
    entry = StudentModel(name=student["name"], department=student["department"])
    key = entry.put()
    list_of_keys.append(key)

def seed_course(course):
    entry = CourseModel(name=course["name"], department=course["department"], student_list=course["student_list"])
    entry.put()

# seed_student({"name": "Joe", "department": "CS"})

def seed_students():
    students = [
        {"name": "Ana", "department": "CS"},
        {"name": "Brenda", "department": "Business"},
        {"name": "Chase", "department": "Metrics"},
        {"name": "Divanka", "department": "CS"}
    ]
    map(lambda student: seed_student(student), students)

def seed_courses():
    courses = [
        {"name": "SW REQs", "department": "CS", "student_list": [list_of_keys[0]]},
        {"name": "Business Analytics", "department": "Business", "student_list": [list_of_keys[0], list_of_keys[1]]},
        {"name": "SW Measurement", "department": "Metrics", "student_list": [list_of_keys[2]]},
        {"name": "SW Architecture", "department": "CS", "student_list": [list_of_keys[0], list_of_keys[1], list_of_keys[3]]}
    ]
    map(lambda course: seed_course(course), courses)

seed_students()
seed_courses()

# [END seeding-data]

# [START main_page]
class MainPage(webapp2.RequestHandler):
    def get(self):
        guestbook_name = self.request.get('guestbook_name',
                                          DEFAULT_GUESTBOOK_NAME)
        greetings_query = Greeting.query(
            ancestor=guestbook_key(guestbook_name)).order(-Greeting.date)
        greetings = greetings_query.fetch(10)

        user = users.get_current_user()
        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'user': user,
            'greetings': greetings,
            'guestbook_name': urllib.quote_plus(guestbook_name),
            'url': url,
            'url_linktext': url_linktext,
        }

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))
# [END main_page]


# [START guestbook]
class Guestbook(webapp2.RequestHandler):
    def post(self):
        # We set the same parent key on the 'Greeting' to ensure each
        # Greeting is in the same entity group. Queries across the
        # single entity group will be consistent. However, the write
        # rate to a single entity group should be limited to
        # ~1/second.
        guestbook_name = self.request.get('guestbook_name',
                                          DEFAULT_GUESTBOOK_NAME)
        greeting = Greeting(parent=guestbook_key(guestbook_name))

        if users.get_current_user():
            greeting.author = Author(
                    identity=users.get_current_user().user_id(),
                    email=users.get_current_user().email())

        greeting.content = self.request.get('content')
        greeting.put()

        query_params = {'guestbook_name': guestbook_name}
        self.redirect('/?' + urllib.urlencode(query_params))
# [END guestbook]

class StudentHandler(webapp2.RequestHandler):
    def get(self):
        name = self.request.get('name', DEFAULT_SCHOOLSYS_NAME)
        # query = StudentModel.query(parent=schoolsys_key(name))
        # print({"student": str(name)})
        student = StudentModel().get_student_info(name)

        self.response.headers['Content-Type'] = 'application/json'

        # self.response.content_type = 'application/json'

        # self.response.write(student)
        self.response.write(json.encode(student))

    def post(self):
        name = self.request.get('name', DEFAULT_SCHOOLSYS_NAME)
        department = self.request.get('department', DEFAULT_SCHOOLSYS_NAME)
        key = StudentModel(name=name, department=department).put()
        self.response.write('successfully added, new studentId=%s' % key.id())

    def put(self):
        id = self.request.get('id', 0)
        id = int(id)
        student = StudentModel().get_a_student_by_id(id)
        if student == None:
            self.response.write('updated failed - no record per studentId %s found!' % id)
        else:
            student.name = self.request.get('name','anonymous')
            student.department = self.request.get('department','No dept')
            student.put()
            self.response.write('studentId %s updated!' % id)

    def delete(self):
        name = self.request.get('name', '')
        res = StudentModel().delete_student(name)
        self.response.write(res)


class CourseHandler(webapp2.RequestHandler):
    def get(self):
        id = self.request.get('id', 0)
        id = int(id)
        key = CourseModel().get_course_by_id(id)
        if key==None:
            self.response.write('No Course matching the query courseId')
        else:
            # key.student_list
            student_list = []
            map(lambda student: self.find_student(student, student_list), key.student_list)
            dict = {
                'id': id,
                'name': key.name,
                'department': key.department,
                'student_list': student_list
            }
            self.response.write(dict)
    def post(self):
        student_keys = []
        student_list = self.request.get('student_list', '')
        if student_list == '':
            print('NO Students')
        else:
            students = json.decode(student_list)
            print(students)
            map(lambda student: student_keys.append(StudentModel(name=student["name"], department=student["department"]).put()), students)

        name = self.request.get('name', 'untitled course')
        department = self.request.get('department', 'no dept.')
        course_key = CourseModel(name=name, department=department, student_list=student_keys).put()
        self.response.write('successfully added, new courseId=%s' % course_key.id())

    def put(self):
        id = self.request.get('id', 0)
        id = int(id)
        course = CourseModel().get_course_by_id(id)
        if course == None:
            self.response.write('updated failed - no record per courseId %s found!' % id)
        else:
            course.name = self.request.get('name','untitled course')
            course.department = self.request.get('department','no dept.')
            student_list = self.request.get('student_list', '')
            if student_list!="":
                student_keys = []
                students = json.decode(student_list)
                map(lambda student: student_keys.append(StudentModel(name=student["name"], department=student["department"]).put()), students)
                course.student_list = student_keys

            course.put()
            self.response.write('courseId %s updated!' % id)

    def delete(self):
        name = self.request.get('name', '')
        res = CourseModel().delete_course(name)
        self.response.write(res)

    def find_student(self, student, student_list):
        id = student.id()
        student_key = StudentModel().get_a_student_by_id(id)
        print("studentId %s, student_key: %s" % (student.id(), student_key))
        if student_key == None:
            return
        else:
            student_list.append(student_key.name)


class StudentIdExtra(webapp2.RequestHandler):
    def get(self):
        name = self.request.get('name', DEFAULT_SCHOOLSYS_NAME)
        student = StudentModel().get_student_id(name)
        self.response.write(student)

# [START app]
app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/sign', Guestbook),
    ('/students', StudentHandler),
    ('/student_id', StudentIdExtra),
    ('/courses', CourseHandler),
], debug=True)
# [END app]
